#!/usr/bin/env python3
import unittest
from typing import List
from collections import Counter, defaultdict

class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        counter = Counter(nums)
        max_item =  max(counter.items(), key=lambda x: x[1])
        return max_item[0]



def solve(nums: List[int]):
    return Solution().majorityElement(nums)


class TestSolution(unittest.TestCase):
    def test_success(self):
        nums = [3, 2, 3]
        result = solve(nums)
        self.assertEqual(3, result)

    def test_success_case2(self):
        nums = [2, 2, 1, 1, 1, 2, 2]
        result = solve(nums)
        self.assertEqual(2, result)


if __name__ == '__main__':
    unittest.main()
