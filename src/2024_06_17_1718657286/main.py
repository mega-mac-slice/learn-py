#!/usr/bin/env python3
import unittest
from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        seen = set()
        to_pop = []
        for i, num in enumerate(nums):
            if num in seen:
                to_pop.append(i)
            else:
                seen.add(num)

        print(to_pop)
        for i in to_pop[::-1]:
            nums.pop(i)

        return len(nums)


def solve(nums: List[int]) -> int:
    return Solution().removeDuplicates(nums)


class TestSolution(unittest.TestCase):
    def test_success(self):
        nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]
        result = solve(nums)
        self.assertEqual(5, result)
        self.assertCountEqual(nums, [0, 1, 2, 3, 4])


if __name__ == '__main__':
    unittest.main()
