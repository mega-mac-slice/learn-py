#!/usr/bin/env python3
import unittest
import math
from typing import Optional, List


class TreeNode:
    def __init__(self, val=0, left: Optional['TreeNode'] = None, right: Optional['TreeNode'] = None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def traverse(self, nums: List[int], start: int, end: int) -> Optional[TreeNode]:
        if start > end:
            return None
        elif start == end:
            return TreeNode(nums[start])
        else:
            midpoint = (start + end) // 2
            node = TreeNode(nums[midpoint])
            node.left = self.traverse(nums, start, midpoint - 1)
            node.right = self.traverse(nums, midpoint + 1, end)

            return node

    def sortedArrayToBST(self, nums: List[int]) -> Optional[TreeNode]:
        if not nums:
            return None
        result = self.traverse(nums, 0, len(nums) - 1)
        return result


def solve(nums: List[int]):
    return Solution().sortedArrayToBST(nums)


class TestSolution(unittest.TestCase):
    def test_success(self):
        nums = [-10, -3, 0, 5, 9]
        result = solve(nums)
        self.assertIsNotNone(result)


if __name__ == '__main__':
    unittest.main()
