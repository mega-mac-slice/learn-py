#!/usr/bin/env python3
import unittest
from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        min_profit = prices[0]
        differences = []

        for _, p1 in enumerate(prices[1::]):
            if p1 < min_profit:
                min_profit = p1
                #print(f"min_profit: {min_profit}")
            else:
                difference = p1 - min_profit
                differences.append(difference)
                #print(f"p1: {p1} min_profit: {min_profit} diff: {difference} diffs: {differences}")

        if not differences:
            return 0

        max_ = max(differences)
        print(f"max: {max_}")
        return max_


def solve(prices: List[int]):
    return Solution().maxProfit(prices)


class TestSolution(unittest.TestCase):
    def test_success(self):
        prices = [7, 1, 5, 3, 6, 4]
        result = solve(prices)
        self.assertIsNotNone(result)
        self.assertEqual(5, result)

    def test_success_case_2(self):
        prices = [7, 6, 4, 3, 1]
        result = solve(prices)
        self.assertIsNotNone(result)
        self.assertEqual(0, result)

    def test_success_case_3(self):
        prices = [1, 2]
        result = solve(prices)
        self.assertIsNotNone(result)
        self.assertEqual(1, result)


if __name__ == '__main__':
    unittest.main()
