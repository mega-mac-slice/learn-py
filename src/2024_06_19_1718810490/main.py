#!/usr/bin/env python3
import unittest
from typing import Optional


class TreeNode:
    def __init__(self, val=0, left: Optional['TreeNode'] = None, right: Optional['TreeNode'] = None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        if p is None and q is None:
            return True
        if p is None or q is None:
            return False
        if p.val != q.val:
            return False

        return self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)


def solve():
    raise NotImplementedError()


class TestSolution(unittest.TestCase):
    def test_success(self):
        result = solve()
        self.assertIsNotNone(result)


if __name__ == '__main__':
    unittest.main()
