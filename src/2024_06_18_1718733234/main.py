#!/usr/bin/env python3
import unittest
from collections import Counter


class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        ransom_counter = Counter(ransomNote)
        magazine_counter = Counter(magazine)

        for k, v in ransom_counter.items():
            if k not in magazine_counter:
                return False
            if v > magazine_counter[k]:
                return False
        return True


def solve(randomNote: str, magazine: str):
    return Solution().canConstruct(randomNote, magazine)


class TestSolution(unittest.TestCase):
    def test_success(self):
        ransomNote = "a"
        magazine = "b"
        result = solve(ransomNote, magazine)
        self.assertFalse(result)

    def test_success_case_2(self):
        ransomNote = "aa"
        magazine = "ab"
        result = solve(ransomNote, magazine)
        self.assertFalse(result)

    def test_success_case_3(self):
        ransomNote = "aa"
        magazine = "aab"
        result = solve(ransomNote, magazine)
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()
