# 2024_06_18_1718733234

More efficient solution:

```python
class Solution:
    def canConstruct(self, s: str, t: str) -> bool:
        l1 = Counter(s)
        l2 = Counter(t)
        if l1 & l2 == l1:
            return 1
        else:
            return 0
```
