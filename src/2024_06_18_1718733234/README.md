# 2024_06_18_1718733234 - Ransom Note

Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters from magazine and false otherwise.

Each letter in magazine can only be used once in ransomNote.

## Reference
- https://leetcode.com/problems/ransom-note/description/?envType=study-plan-v2&envId=top-interview-150