#!/usr/bin/env python3
import unittest
from typing import Optional, List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left: Optional['TreeNode'] = None, right: Optional['TreeNode'] = None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def collect(self, node: Optional[TreeNode], traversed_values: set, diffs: set):
        if not node:
            return
        # print(self.traversed_values)
        for x in traversed_values:
            diff = abs(x - node.val)
            # print(f"x: {x}, node.val: {node.val}, diff: {diff}")
            if diff:
                diffs.add(diff)

        traversed_values.add(node.val)

        self.collect(node.left, traversed_values, diffs)
        self.collect(node.right, traversed_values, diffs)

    def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
        traversed_values = set()
        diffs = set()
        self.collect(root, traversed_values, diffs)
        print(traversed_values)
        print(diffs)
        return min(diffs)


def solve(root: Optional[TreeNode]) -> int:
    return Solution().getMinimumDifference(root)


class TestSolution(unittest.TestCase):
    def test_success(self):
        result = solve(None)
        self.assertIsNotNone(result)


if __name__ == '__main__':
    unittest.main()
