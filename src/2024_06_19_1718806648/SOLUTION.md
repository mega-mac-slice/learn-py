# 2024_06_19_1718806648

More efficient solution:

```python
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
        def helper(root):
            answers.append(root.val)
            if root.left:
                helper(root.left)
            if root.right:
                helper(root.right)
        if not root:
            return 0
        answers = []
        helper(root)
        answers = sorted(answers)
        n = len(answers)
        final = []
        for i in range(1, n):
            final.append(answers[i] - answers[i-1])
        return min(final)
```
