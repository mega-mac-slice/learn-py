#!/usr/bin/env python3
import unittest
from typing import List


class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        good_indices = []
        for i, num in enumerate(nums):
            if num != val:
                good_indices.append(i)

        count = len(good_indices)
        print(f"good_indices: {good_indices} count: {count} nums: {nums}")

        index = 0
        while good_indices:
            good_index = good_indices.pop(0)
            nums[index] = nums[good_index]
            index += 1

        for i in range(index, len(nums)):
            nums.pop(-1)

        print(nums)
        return count


def solve(nums: List[int], val: int) -> int:
    return Solution().removeElement(nums, val)


class TestSolution(unittest.TestCase):
    def test_success(self):
        nums = [3, 2, 2, 3]
        val = 3
        result = solve(nums, val)
        self.assertCountEqual(nums, [2, 2])
        self.assertEqual(result, 2)

    def test_success_case2(self):
        nums = [0, 1, 2, 2, 3, 0, 4, 2]
        val = 2
        result = solve(nums, val)
        self.assertCountEqual(nums, [0, 1, 4, 0, 3])
        self.assertEqual(result, 5)


if __name__ == '__main__':
    unittest.main()
