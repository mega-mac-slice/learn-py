# 2024_06_19_1718809831 - Invert Binary Tree

Given the root of a binary tree, invert the tree, and return its root.


## Reference 
- https://leetcode.com/problems/invert-binary-tree/description/?envType=study-plan-v2&envId=top-interview-150
