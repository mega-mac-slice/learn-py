#!/usr/bin/env python3
import unittest
from typing import Optional


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left: Optional['TreeNode'] = None, right: Optional['TreeNode'] = None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        if root is None:
            return

        root.left, root.right = root.right, root.left
        self.invertTree(root.left)
        self.invertTree(root.right)

        return root


def solve(root: Optional[TreeNode]) -> Optional[TreeNode]:
    return Solution().invertTree(root)

class TestSolution(unittest.TestCase):
    def test_success(self):
        result = solve(None)
        self.assertIsNotNone(result)


if __name__ == '__main__':
    unittest.main()
