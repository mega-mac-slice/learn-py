#!/usr/bin/env python3
import unittest
from typing import List


class Solution:

    def shift_by_1(self, nums1: List[int], index: int) -> None:
        for i in range(len(nums1) - 1, index, -1):
            nums1[i] = nums1[i - 1]

    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        i = 0
        j = 0

        for k in range(m, m + n):
            nums1[k] = None

        for _ in range(m + n):
            if (i >= m + n) or (j >= n):
                break

            val1 = nums1[i]
            val2 = nums2[j]
            print(f"i: {i} j: {j} nums1: {nums1} nums2: {nums2}")

            if val1 is None:
                nums1[i] = val2
                i += 1
                j += 1
            elif val1 > val2:
                self.shift_by_1(nums1, i)
                nums1[i] = val2
                i += 1
                j += 1
            else:
                i += 1


def solve(nums1: List[int], m: int, nums2: List[int], n: int) -> None:
    Solution().merge(nums1, m, nums2, n)


class TestSolution(unittest.TestCase):
    def test_success(self):
        nums1 = [1, 2, 3, 0, 0, 0]
        m = 3
        nums2 = [2, 5, 6]
        n = 3

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [1, 2, 2, 3, 5, 6])

    def test_success_append(self):
        nums1 = [1, 0]
        m = 1
        nums2 = [2]
        n = 1

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [1, 2])

    def test_success_empty_nums1(self):
        nums1 = [0]
        m = 0
        nums2 = [1]
        n = 1

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [1])

    def test_success_insert(self):
        nums1 = [1, 3, 0]
        m = 2
        nums2 = [2]
        n = 1

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [1, 2, 3])

    def test_success_insert_from_nums1(self):
        nums1 = [4, 0, 0, 0, 0, 0]
        m = 1
        nums2 = [1, 2, 3, 5, 6]
        n = 5

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [1, 2, 3, 4, 5, 6])

    def test_success_insert_nums2_before_nums1(self):
        nums1 = [4, 5, 6, 0, 0, 0]
        m = 3
        nums2 = [1, 2, 3]
        n = 3

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [1, 2, 3, 4, 5, 6])

    def test_success_handle_negatives_and_actual_zeros(self):
        nums1 = [-1, 0, 0, 3, 3, 3, 0, 0, 0]
        m = 6
        nums2 = [1, 2, 2]
        n = 3

        solve(nums1, m, nums2, n)
        print(nums1)
        self.assertListEqual(nums1, [-1, 0, 0, 1, 2, 2, 3, 3, 3])


if __name__ == '__main__':
    unittest.main()
