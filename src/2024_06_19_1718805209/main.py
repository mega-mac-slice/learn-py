#!/usr/bin/env python3
import unittest
from collections import deque


class Solution:
    def isValid(self, s: str) -> bool:
        queue = deque()

        pairs = {
            "(": ")",
            "[": "]",
            "{": "}"
        }

        if len(s) % 2 != 0:
            return False

        for x in s:
            if x in pairs.keys():
                queue.append(x)
            if x in pairs.values():
                if not queue:
                    return False
                top = queue.pop()
                compliment = pairs[top]
                if x != compliment:
                    return False

        return not queue


def solve(s: str) -> bool:
    return Solution().isValid(s)


class TestSolution(unittest.TestCase):
    def test_success(self):
        s = "()"
        result = solve(s)
        self.assertTrue(result)

    def test_success_2(self):
        s = "()[]{}"
        result = solve(s)
        self.assertTrue(result)

    def test_success_3(self):
        s = "(]"
        result = solve(s)
        self.assertFalse(result)

    def test_success_4(self):
        s = "["
        result = solve(s)
        self.assertFalse(result)

    def test_success_5(self):
        s = "(("
        result = solve(s)
        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()
