# py-learn
Generic repository for practicing problems in python.

[Index](./INDEX.md)

## Training
- [Codewars](https://www.codewars.com/)
- [LeetCode](https://leetcode.com)

## Reference
- [Big-O Cheat Sheet](http://bigocheatsheet.com/)
- [LeetCode: Top Interview 150](https://leetcode.com/studyplan/top-interview-150/)