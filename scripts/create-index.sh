#!/usr/bin/env bash

echo "# Index" > ./INDEX.md

problems=($(ls src))

for dir in "${problems[@]}"
do
    path="./src/${dir}"
    line=$(awk 'NR==1 {print;}' ${path}/README.md)
    heading=${line:2}
    echo "- [${heading}](${path})" >> ./INDEX.md
done